import os
from datetime import timedelta, datetime
from functools import partial

import telebot
from telebot import types
import pymongo

API_KEY = os.environ.get('TG_API_KEY')
ADMIN_CHAT_ID = int(os.environ.get('ADMIN_CHAT_ID'))

MONGODB_LINK = os.environ.get('MONGODB_LINK')
MONGO_DB = os.environ.get('MONGO_DB')

bot = telebot.TeleBot(API_KEY)

client = pymongo.MongoClient(MONGODB_LINK)
db = client['tg-bot']
collection = db["messages"]

msg = db["main_message"]
MESSAGE = msg.find_one({"_id": 'main_message'})
if not MESSAGE:
    MESSAGE = 'Технические работы!'
    msg.insert_one({'_id': 'main_message', 'text': MESSAGE})


def change_name(message):
    global MESSAGE
    MESSAGE = message.text
    data = {'_id': 'main_message', 'text': message.text}
    msg.update_one({'_id': 'main_message'}, {'$set': data}, upsert=True)
    bot.send_message(ADMIN_CHAT_ID, text=f"Сообщение изменено на: {MESSAGE}", timeout=1)


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(ADMIN_CHAT_ID, f"@{message.from_user.username} {message.from_user.first_name} "
                                    f"[{message.from_user.id}] нажал(-а) старт: "
                                    f"{datetime.now() + timedelta(hours=7)}")
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button = types.KeyboardButton("Получить информационное сообщение")
    markup.add(button)
    bot.send_message(message.chat.id, text="Хай!", reply_markup=markup, timeout=1)


@bot.message_handler(commands=['change_msg'], func=lambda message: True)
def start(message):
    if message.chat.id == ADMIN_CHAT_ID:
        bot.send_message(ADMIN_CHAT_ID, text="Введи сообщение", timeout=1)
        bot.register_next_step_handler(message, partial(change_name))


@bot.message_handler(commands=['drop_user_from_db'], func=lambda message: True)
def drop_user_from_db(message):
    args = message.text.split()[1:]
    if args and message.chat.id == ADMIN_CHAT_ID:
        collection.delete_one({'_id': int(args[0])})
        bot.send_message(ADMIN_CHAT_ID, text="Данные успешно удалены", timeout=1)


def insert_user_in_db_msg(message, args):
    data = {'_id': int(args[0]), 'text': message.text}
    collection.update_one({'_id': int(args[0])}, {'$set': data}, upsert=True)
    bot.send_message(ADMIN_CHAT_ID, text=f"Сообщение изменено на: {message.text}", timeout=1)


@bot.message_handler(commands=['insert_user_in_db'], func=lambda message: True)
def insert_user_in_db(message):
    args = message.text.split()[1:]
    if args and message.chat.id == ADMIN_CHAT_ID:
        bot.send_message(ADMIN_CHAT_ID, text="Введи сообщение", timeout=1)
        bot.register_next_step_handler(message, partial(insert_user_in_db_msg, args=args))


@bot.message_handler(content_types=["text"])
def send_message(message):
    if message.text == "Получить информационное сообщение":
        user_msg = collection.find_one({"_id": message.from_user.id})
        if not user_msg:
            data = {'_id': message.from_user.id, 'text': MESSAGE}
            collection.insert_one(data)
            bot.send_message(message.from_user.id, f"{MESSAGE}", timeout=1)
        else:
            bot.send_message(message.from_user.id, f"{user_msg['text']}", timeout=1)

        bot.send_message(ADMIN_CHAT_ID, f"@{message.from_user.username} {message.from_user.first_name} "
                                        f"[{message.from_user.id}] получил(-а) сообщение: "
                                        f"{datetime.now() + timedelta(hours=7)}")

    elif message.chat.id != ADMIN_CHAT_ID:
        bot.forward_message(ADMIN_CHAT_ID, message.chat.id, message.id)


@bot.message_handler(content_types=['photo'])
def photo(message):
    if message.chat.id != ADMIN_CHAT_ID:
        bot.forward_message(ADMIN_CHAT_ID, message.chat.id, message.id)


while True:
    bot.polling(none_stop=True, timeout=123)
