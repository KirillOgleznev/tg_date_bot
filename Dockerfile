FROM python:3.11

RUN pip install pyTelegramBotAPI
RUN pip install pymongo

ADD ./ /opt
WORKDIR /opt

ENTRYPOINT python main.py
